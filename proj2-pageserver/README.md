# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

### What is this repository for? ###
it creates a small server on the machine that runs this code that makes a web page that can access html files in a folder.

It was created to help CIS students gain
  * Experience with GIT workflow with separate configuration: Fork the project, make and test changes locally, commit; turn in configuration file with reference to repo. (Project 0 is practice for this part.) 
  * Extend a tiny web server in Python, to check understanding of basic web architecture
  * Use automated tests to check progress (plus manual tests for good measure)
  * and lastly to try using flask to create a server

### What do I need?  Where will it work? ###

* Designed for Unix, mostly interoperable on Linux (Ubuntu) or MacOS. May also work on Windows, but no promises. A Linux virtual machine may work, but our experience has not been good; you may want to test on shared server ix-dev.

* You will need docker to create the image and run the container 

* Designed to work in "user mode" (unprivileged), therefore using a port number above 1000 (rather than port 80 that a privileged web server would use)

  ```
  ## Author: Genevieve Dorrell, gdorrel2@uoregon.edu ##
  * source code from professor Ram Durairajan
  ```
  
deployment should work with this command sequence on a unix terminal

  ```
$ git clone <yourGitRepository> <targetDirectory>
$ cd <targetDirectory>
$ cd web/
$ sudo docker build -t uocis-flask-demo .
$ sudo docker run -d -p 5000:5000 uocis-flask-demo

make sure to stop and remove the container when done
  
  ```



